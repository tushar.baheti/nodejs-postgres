const express = require('express');
const app = express();
const pool = require('./db');

app.use(express.json());



//ROUTES


//get all todos
app.get('/todos', async (req, res) => {
    try {

        const alltodos = await pool.query("SELECT *FROM todo");

        res.json(alltodos.rows);

    } catch (error) {

        console.log(error);

    }
})



//create a todo
app.post('/todos', async (req, res) => {
    try {

        const { description } = req.body;
        const newTodo = await pool.query("INSERT INTO todo (description) VALUES ($1) RETURNING *", [description]
        );

        res.json(newTodo.rows[0]);

    } catch (error) {
        console.log(error);
    }
})

//get a todo
app.get('/todos/:id', async (req, res) => {

    const { id } = req.params;

    try {
        const todo = await pool.query("SELECT * FROM todo WHERE todo_id=$1", [id]);

        res.json(todo.rows[0]);

    } catch (error) {

        console.log(error);

    }

})



//update a todo
app.put("/todos/:id", async (req, res) => {

    try {
        const { id } = req.params;  //where
        const { description } = req.body;  //set

        const updateTodo = await pool.query("UPDATE todo SET description = $1 WHERE todo_id = $2", [description, id])

        res.json("Todo was updated: ");

    } catch (error) {

    }

})



//delete a todo
app.delete('/todos/:id', async (req, res) => {

    try {
        const { id } = req.params;
        const deleteTodo = await pool.query("DELETE FROM todo WHERE todo_id=$1", [id]);

        res.json('Todo was successfully deleted');

    } catch (error) {

        console.log(error);

    }

})




app.listen(3000, () => {
    console.log('Server is listening on port 3000');
})