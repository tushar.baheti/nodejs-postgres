CREATE DATABASE test;


-- \c into database


CREATE TABLE todo(
    todo_id SERIAL PRIMARY KEY,
    description VARCHAR(255)
);



CREATE TABLE student(
    student_id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    age INT,
    city VARCHAR(255),
    salary INT
);


INSERT INTO student(1, "Bunny", 25, "Bangalore", 25000);


CREATE DATABASE product;


--Check constraints

CREATE TABLE products(
product_no integer,
name text,
price numeric CHECK (price>0)
);


CREATE TABLE products(
product_no integer,
name text,
price numeric CHECK (price>0),
discounted_price numeric CHECK(discounted_price>0),
CHECK(price > discounted_price)
);




--not null
CREATE TABLE products(
    product_no integer NOT NULL,
    name text NOT NULL,
    price numeric NOT NULL CHECK(price>0)
);


--unique
CRETAE TABLE products(
    product_no integer UNIQUE,
    name text NOT NULL,
    price numeric
);

OR

CRETAE TABLE products(
    product_no integer,
    name text NOT NULL,
    price numeric,
    UNIQUE (product_no, name)
);



--primary key
CREATE TABLE products(
product_no integer PRIMARY KEY,
name text,
price numeric
);



--foreign key

CREATE TABLE orders(
order_id integer PRIMARY KEY,
product_no integer REFERENCES products(product_no),
quantity integer
);


CREATE TABLE orders(
    order_id integer PRIMARY KEY,
    address text
);


CREATE TABLE order_items(
    product_no integer REFERENCES products(product_no),
    order_id integer REFERENCES orders(order_id),
    quantity integer,
    PRIMARY KEY(product_no, order_id)
);


CREATE TABLE order_items(
    product_no integer REFERENCES products(product_no) ON DELETE RESTRICT,
    --if there is order then person cannot delete main product

    order_id integer REFERENCES orders(order_id) ON DELETE CASCADE,
    --main order is deleted all the items in order is deleted
    --delets all the refernces in the table


    quantity integer,
    PRIMARY KEY(product_no, order_id)
);



CREATE TABLE order_items(
    product_no integer REFERENCES products(product_no) ON DELETE RESTRICT,
    order_id integer REFERENCES orders(order_id) ON DELETE CASCADE,
    quantity integer,
    PRIMARY KEY(product_no, order_id)
);




--alter table
ALTER TABLE student ADD gender char(1);



--drop table
ALTER TABLE student DROP COLUMN gender;



--add delete primary key programmatically
alter table student ADD CONSTRAINT pri PRIMARY KEY(student_id);

alter table student DROP CONSTRAINT pri;



--update and delete in table

update student set age=35 where student_id=3;

delete from student where student_id=2;



--limit and offset

select * from student limit 3;

select * from student limit 4 offset 2;


--group by

select age count(*) from student group by age;

--having clause for aggrgation used with group by

select age count(*) from student group by age having MAX(salary)>15000;



---order by clause

select * from student order by age ASC;
select * from student order by age DESC;

--order by multiple values
--here age is asc default and salary desc;
select * from student order by age, salary DESC;



--enum datatype

create table mood as ENUM('sad', 'fine', 'happy');

create table person(
    name text,
    current_mood mood
);



-- working with json data
CREATE TABLE jsondata(
    id integer,
    doc JSON
);


insert into jsondata values(1, '{
"name": "Akhil",
"address": {
    "lane1": "Lapjapt nagar",
    "lane2": "new Delhi",
    "pincode": "110048"
}    
}');


select doc->>'address' from jsondata;

select doc->'address'->>'pincode' from jsondata;


select doc#>>'{address, pincode}' from jsondata;


CREATE TABLE sal_emp(
    name text,
    pay integer[],
    schedule text[][]
);

insert into sal_emp values (
    'Rahul', 
    '{500, 1000, 1500, 2000}',
    '{{"meeting", "lunch"}, {"training", "presentation"}}'
);


--1 based indexing 
select pay[2] from sal_emp;

select name from sal_emp where pay[1]<>pay[2];

select array_dims(schedule) from sal_emp where name='Anish';

update sal_emp set pay='{2500, 5000, 7500, 10000}'  where name='Rahul';

update sal_emp set pay[3]=90000 where name ='Rahul';


update sal_emp set pay[1:2]='{20000, 20000}';

--add single values to array
update sal_emp set pay=array_append(pay, 105000);

--add and update multiple values in array
update sal_emp set pay=array_cat(pay, ARRAY[100,200]);


update sal_emp set pay=array_remove(pay, 20000);


--search in array
select * from sal_emp where 10000=ANY(pay);


select * from sal_emp where 25000=ALL(pay);